import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
import os
import time
import datetime
from torch.autograd import grad
from torch.autograd import Variable
from torchvision.utils import save_image
from torchvision import transforms
from model import *
from PIL import Image, ImageDraw, ImageFont
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from utils import *
from data_loader import *
from tqdm import tqdm
import models.cifar10 as cifar10models
import models

model_names = sorted(name for name in models.__dict__
                     if name.islower() and not name.startswith("__")
                     and callable(models.__dict__[name]))

cifar10_names = sorted(name for name in cifar10models.__dict__
                     if name.islower() and not name.startswith("__")
                     and callable(cifar10models.__dict__[name]))
model_names = cifar10_names + model_names

arch = "preact_resnet50"
model = cifar10models.__dict__[arch] if arch in cifar10_names else models.__dict__[arch] 

print("=> using pre-trained model '{}'".format(arch))
model = model()