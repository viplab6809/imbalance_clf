import os
import argparse
from solver import Solver
from data_loader import *
from torch.backends import cudnn
from utils import *
from datetime import datetime
import zipfile

def zipdir(path, ziph):
    files = os.listdir(path)
    for file in files:
        if file.endswith(".py") or file.endswith("cfg"):
            ziph.write(os.path.join(path, file))
            if file.endswith("cfg"):
                os.remove(file)

def save_config(config):
    current_time = str(datetime.now()).replace(":","_")
    save_name= "scganv2_files_{}.{}"
    with open(save_name.format(current_time,"cfg"),"w") as f:
        for k, v in sorted(args.items()):
            f.write('%s: %s\n' % (str(k), str(v)))
    
    
    
    zipf = zipfile.ZipFile(save_name.format(current_time,"zip"), 'w', zipfile.ZIP_DEFLATED)
    zipdir('.', zipf)
    zipf.close()
    
def str2bool(v):
    return v.lower() in ('true')

def main(config):
    # For fast training
    cudnn.benchmark = True

    # Make the path absolute path
    cur_dir = os.path.dirname(os.path.abspath(__file__))
    config.log_path = cur_dir+"/"+config.prefix+config.log_path
    config.model_save_path = cur_dir+"/"+config.prefix+config.model_save_path

    # Create directories if not exist
    mkdir(config.log_path)
    mkdir(config.model_save_path)
    
    train_data_loader = get_loader("C:/Datasets/cifar10-train-50000", img_size=config.img_size, batch_size=config.batch_size, mode="train")
    val_data_loader = get_loader("C:/Datasets/cifar10-test-10000", img_size=config.img_size, batch_size=config.batch_size, mode="val")
    data_loaders = ExpDataLoaders(train_loader=train_data_loader, val_loader=val_data_loader)
    # Solver
    solver = Solver(vars(config), data_loaders)

    if config.mode == 'train':
        solver.train()
    elif config.mode == 'test':
        solver.test()
    elif config.mode == 'lr_find':
        solver.lr_finder()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    # Model hyper-parameters
    parser.add_argument('--lr', type=float, default=1.2)
    parser.add_argument('--lr_div', type=float, default=30)
    parser.add_argument('--pct', type=float, default=20)
    parser.add_argument('--max_momentum', type=float, default=0.95)
    parser.add_argument('--min_momentum', type=float, default=0.85)
    parser.add_argument('--weight_decay', type=float, default=2e-4)
    parser.add_argument('--img_size', type=int, default=32)

    
    parser.add_argument('--use_coord', type=str2bool, default=False)
    parser.add_argument('--use_spectral_norm', type=str2bool, default=False)
    parser.add_argument('--normalizer', type=str, default='batch', choices=['instance', 'batch', 'conditional_batch'])

    # Training settings
    parser.add_argument('--num_epochs', type=int, default=65)
    parser.add_argument('--batch_size', type=int, default=256)
    parser.add_argument('--batch_accumulation', type=int, default=1)
    parser.add_argument('--n_output', type=int, default=10)
    parser.add_argument('--pretrained_model', type=str, default=None)
    parser.add_argument('--freeze_layers', type=str2bool, default=False)
    parser.add_argument('--fp16', type=str2bool, default=False)
    parser.add_argument('--loss_scale', type=float, default=256)
    
    # Misc
    parser.add_argument('--mode', type=str, default='train', choices=['train', 'test', 'lr_find'])
    parser.add_argument('--use_tensorboard', type=str2bool, default=True)

    # Path
    parser.add_argument('--prefix', type=str, default='ImbaCLF')
    parser.add_argument('--log_path', type=str, default='/logs')
    parser.add_argument('--model_save_path', type=str, default='/models')

    # Step size
    parser.add_argument('--log_step', type=int, default=100)
    parser.add_argument('--sample_step', type=int, default=500)
    parser.add_argument('--model_save_step', type=int, default=5000)

    config = parser.parse_args()
 
    args = vars(config)
    print('------------ Options -------------')
    for k, v in sorted(args.items()):
        print('%s: %s' % (str(k), str(v)))
    print('-------------- End ----------------')

    # save_config(config)
    main(config)