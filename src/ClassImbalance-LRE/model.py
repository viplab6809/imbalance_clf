import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
import torchvision
from torch.autograd import Variable
import itertools
from utils import *
from layers import *
import argparse
import models.cifar10 as cifar10models
import models

model_names = sorted(name for name in models.__dict__
                     if name.islower() and not name.startswith("__")
                     and callable(models.__dict__[name]))

cifar10_names = sorted(name for name in cifar10models.__dict__
                     if name.islower() and not name.startswith("__")
                     and callable(cifar10models.__dict__[name]))

model_names = cifar10_names + model_names

def children(m): return m if isinstance(m, (list, tuple)) else list(m.children())

def num_features(m):
    c=children(m)
    if len(c)==0: return None
    for l in reversed(c):
        if hasattr(l, 'num_features'): 
            return l.num_features
        res = num_features(l)
        if res is not None: return res

class AdaptiveConcatPool2d(nn.Module):
    def __init__(self, sz=None, adaptive_pool=["max","avg"]):
        super().__init__()
        sz = sz or (1,1)
        self.ap = nn.AdaptiveAvgPool2d(sz)
        self.mp = nn.AdaptiveMaxPool2d(sz)
        self.adaptive_pool = adaptive_pool

    def forward(self, x): 
        layers = []
        if isinstance(self.adaptive_pool, (list, tuple)):
            for l in self.adaptive_pool:
                if l == "max":
                    layers.append(self.mp(x))
                else:
                    layers.append(self.ap(x))
        else:
            if l == "max":
                layers.append(self.mp(x))
            else:
                layers.append(self.ap(x))

        return torch.cat(layers, 1)

class Lambda(nn.Module):
    def __init__(self, f): super().__init__(); self.f=f
    def forward(self, x): return self.f(x)

class Flatten(nn.Module):
    def __init__(self): super().__init__()
    def forward(self, x): return x.view(x.size(0), -1)


class DefaultClassifiers(nn.Module):
    DEFAULTS = {}
    model_meta = {
        'resnet18':[8,6], 'resnet34':[8,6], 'resnet50':[8,6], 'resnet101':[8,6], 'resnet152':[8,6],
        'preact_resnet18':[8,6], 'preact_resnet34':[8,6], 'preact_resnet50':[5,6], 'preact_resnet101':[8,6], 'preact_resnet152':[8,6],
        'vgg16':[0,22], 'vgg19':[0,22],
        'resnext50':[8,6], 'resnext101':[8,6], 'resnext101_64':[8,6],
        'wrn':[8,6], 'inceptionresnet_2':[-2,9], 'inception_4':[-1,9],
        'dn121':[0,7], 'dn161':[0,7], 'dn169':[0,7], 'dn201':[0,7],
    }

    model_features = {'preact_resnet18':512, 'preact_resnet34':512, 'preact_resnet50':2048, 'preact_resnet101':2048, 'preact_resnet152':2048,
                    'inception_4': 3072//2, 'dn121': 2048//2, 'dn161': 4416//2,}
                    
    def __init__(self, architecture='resnet50', pretrained=False, n_output=10, n_cls_layers=2, adaptive_pool=["max", "avg"], dropout_prob=None):
        super(DefaultClassifiers, self).__init__()
        # self.__dict__.update(DefaultClassifiers.DEFAULTS, **config)

        if architecture not in model_names:
            print("Architecture not recognized, defaulting to ResNet50")
            architecture = 'resnet50'

        model = cifar10models.__dict__[architecture] if architecture in cifar10_names else models.__dict__[architecture]
        if pretrained:
            model = model(pretrained=True)
        else:
            model = model()

        last_layer_idx, lr_group_cut = self.model_meta[architecture]
        self.base_layers = nn.Sequential(*self.cut_layers(model, last_layer_idx))


        n_features = self.model_features[architecture] if architecture in self.model_features else (num_features(self.base_layers))
        # print(self.base_layers)

        # if model == "resnet34":
        #     self.base_layers = nn.Sequential(*self.getResNet34(pretrained))
        # elif model == "resnet18":
        #     self.base_layers = nn.Sequential(*self.getResNet18(pretrained))
        # elif model == "resnext50":
        #     self.base_layers = nn.Sequential(*self.getResNext50(pretrained))

        self.f_classifier = nn.Sequential(*self.create_classifier_layers(n_features, n_output, n_cls_layers, adaptive_pool, dropout_prob))
        # print(self.f_classifier)
        last_layer_idx, lr_group_cut = self.model_meta[architecture]
        self.layer_groups = self.get_layer_groups(lr_group_cut) + [self.f_classifier]

        # self.box_prior_h = 1
        # self.box_prior_w = 1
        # self.param_groups = self.create_param_groups(self.layer_groups, lr_list)
        # self.main = nn.Sequential(*self.layer_groups)
    def create_classifier_layers(self, n_features, n_output, n_cls_layers=1, adaptive_pool=["max", "avg"], dropout_prob=None):
        layers = []

        if adaptive_pool:
            layers.append(AdaptiveConcatPool2d(adaptive_pool=adaptive_pool))
            layers.append(Flatten())

            feat_mult = len(adaptive_pool) if isinstance(adaptive_pool, (list, tuple)) else 1

            in_feat = n_features * feat_mult
            out_feat = in_feat // feat_mult
            for l in range(n_cls_layers-1):
                if l == 0:
                    layers.append(nn.BatchNorm1d(in_feat, affine=True))
        
                    if dropout_prob is not None:
                        if isinstance(dropout_prob, (list,tuple)):
                            layers.append(nn.Dropout(p=dropout_prob[0]))
                        else:
                            layers.append(nn.Dropout(p=dropout_prob))
                layers.append(nn.Linear(in_feat, out_feat, bias=True))
                layers.append(nn.ReLU(inplace=True))

                in_feat = out_feat
                layers.append(nn.BatchNorm1d(in_feat, affine=True))
    
                if dropout_prob is not None:
                    if isinstance(dropout_prob, (list,tuple)):
                        layers.append(nn.Dropout(p=dropout_prob[l]))
                    else:
                        layers.append(nn.Dropout(p=dropout_prob))

                

            layers.append(nn.Linear(in_feat, n_output, bias=True))

        else:
            layers.append(nn.BatchNorm2d(n_features, affine=True))

            if dropout_prob is not None:
                if isinstance(dropout_prob, (list,tuple)):
                    layers.append(nn.Dropout(p=dropout_prob[0]))
                else:
                    layers.append(nn.Dropout(p=dropout_prob))
            layers.append(nn.Conv2d(n_features, n_features // 2, kernel_size=3, stride=1, padding=1, bias=False))
            layers.append(nn.ReLU(inplace=True))
            layers.append(nn.BatchNorm2d(n_features // 2, affine=True))

            if dropout_prob is not None:
                if isinstance(dropout_prob, (list,tuple)):
                    layers.append(nn.Dropout(p=dropout_prob[1]))
                else:
                    layers.append(nn.Dropout(p=dropout_prob))

            layers.append(nn.Conv2d(n_features // 2, n_output, kernel_size=1, stride=1, padding=0, bias=True))

        return layers

    def getResNet34(self, pretrained=True):
        model = torchvision.models.resnet34(pretrained=pretrained)

        last_layer_idx, lr_group_cut = self.model_meta["resnet34"]

        return self.cut_layers(model,last_layer_idx)    


    def getResNet18(self, pretrained=True):
        model = torchvision.models.resnet18(pretrained=pretrained)

        last_layer_idx, lr_group_cut = self.model_meta["resnet18"]

        return self.cut_layers(model,last_layer_idx)    

    def getResNext50(self, pretrained=True, weights_path="./models/resnext_50_32x4d.pth"):
        model = resnext_50_32x4d()
        if pretrained:
            model.load_state_dict(torch.load(weights_path))
        last_layer_idx, lr_group_cut = self.model_meta["resnext50"]

        return self.cut_layers(model,last_layer_idx)

    def cut_layers(self, m, cut):
        return list(m.children())[:cut] if cut else [m]

    def get_trainable_params(self,m=None):
        if m is None:
            m = self
        return [p for p in m.parameters() if p.requires_grad]

    def chain_params(self,p):
        if isinstance(p, (list,tuple)):
            return list(itertools.chain(*[self.get_trainable_params(o) for o in p]))
        return self.get_trainable_params(p)

    def create_param_groups(self,layer_groups, lr_list):
        return [{'params': self.chain_params(pg), 'lr':lr} for pg,lr in zip(layer_groups,lr_list)]

    def get_layer_groups(self,lr_cut):
        layer_groups = []
        layers = list(self.base_layers.children())

        if isinstance(lr_cut,int):
            layer_groups.append(layers[:lr_cut])
            layer_groups.append(layers[lr_cut:])
            return layer_groups
        
        if isinstance(lr_cut,(list,tuple)):
            last_idx = 0
            for i in lr_cut:
                layer_groups.append(layers[last_idx:i])
                last_idx = i
            layer_groups.append(layers[last_idx:])

        return layer_groups
    def freeze(self):
        self.freeze_to(-1)
    def freeze_to(self, n):
        c=self.layer_groups
        for l in c:     
            self.set_trainable(l, False)
        for l in c[n:]: 
            self.set_trainable(l, True)

    def get_children(self, m): 
        return m if isinstance(m, (list, tuple)) else list(m.children())
    def unfreeze(self): self.freeze_to(0)

    def set_trainable_attr(self, m,b):
        m.trainable=b
        for p in m.parameters(): 
            p.requires_grad=b

    def apply_leaf(self,m, f):
        c = self.get_children(m)
        if isinstance(m, nn.Module): f(m)
        if len(c)>0:
            for l in c: self.apply_leaf(l,f)

    def set_trainable(self, l, b):
        self.apply_leaf(l, lambda m: self.set_trainable_attr(m,b))

    def forward(self, x):
        net = self.base_layers(x)
        return self.f_classifier(net)


class DefaultClassifiersFixed(nn.Module):
    DEFAULTS = {}
    model_meta = {
        'resnet18':[8,6], 'resnet34':[8,6], 'resnet50':[8,6], 'resnet101':[8,6], 'resnet152':[8,6],
        'preact_resnet18':[8,6], 'preact_resnet34':[8,6], 'preact_resnet50':[5,6], 'preact_resnet101':[8,6], 'preact_resnet152':[8,6],
        'vgg16':[0,22], 'vgg19':[0,22],
        'resnext50':[8,6], 'resnext101':[8,6], 'resnext101_64':[8,6],
        'wrn':[8,6], 'inceptionresnet_2':[-2,9], 'inception_4':[-1,9],
        'dn121':[0,7], 'dn161':[0,7], 'dn169':[0,7], 'dn201':[0,7],
    }

    model_features = {'preact_resnet18':512, 'preact_resnet34':512, 'preact_resnet50':2048, 'preact_resnet101':2048, 'preact_resnet152':2048,
                    'inception_4': 3072//2, 'dn121': 2048//2, 'dn161': 4416//2,}
                    
    def __init__(self, architecture='resnet50', pretrained=False):
        super(DefaultClassifiersFixed, self).__init__()
        # self.__dict__.update(DefaultClassifiers.DEFAULTS, **config)

        if architecture not in model_names:
            print("Architecture not recognized, defaulting to ResNet50")
            architecture = 'resnet50'

        model = cifar10models.__dict__[architecture] if architecture in cifar10_names else models.__dict__[architecture]
        if pretrained:
            model = model(pretrained=True)
        else:
            model = model()

        self.model = model

    def get_trainable_params(self,m=None):
        if m is None:
            m = self
        return [p for p in m.parameters() if p.requires_grad]

    def forward(self, x):
        return self.model(x) 


class LeNet(MetaModule):
    def __init__(self, n_out):
        super(LeNet, self).__init__()
    
        layers = []
        layers.append(MetaConv2d(1, 6, kernel_size=5))
        layers.append(nn.ReLU(inplace=True))
        layers.append(nn.MaxPool2d(kernel_size=2,stride=2))

        layers.append(MetaConv2d(6, 16, kernel_size=5))
        layers.append(nn.ReLU(inplace=True))
        layers.append(nn.MaxPool2d(kernel_size=2,stride=2))
        
        layers.append(MetaConv2d(16, 120, kernel_size=5))
        layers.append(nn.ReLU(inplace=True))
        
        self.main = nn.Sequential(*layers)
        
        layers = []
        layers.append(MetaLinear(120, 84))
        layers.append(nn.ReLU(inplace=True))
        layers.append(MetaLinear(84, n_out))
        
        self.fc_layers = nn.Sequential(*layers)
        
    def forward(self, x):
        x = self.main(x)
        x = x.view(-1, 120)
        return self.fc_layers(x).squeeze()


def str2bool(v):
    return v.lower() in ('true')


if __name__ == '__main__':
    model = DefaultClassifiersFixed(architecture="preact_resnet_meta50")
    
    inp = torch.randn(16,3,32,32)

    out = model(inp)
    print(out.size())
    print(out)