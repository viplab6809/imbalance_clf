import torch
import os
import random
from torch.utils.data import Dataset
from torch.utils.data import DataLoader
from torchvision import transforms
from torchvision.datasets import ImageFolder
import matplotlib.pyplot as plt
from PIL import Image
import h5py
import numpy as np
import collections
import numbers
import math
import pandas as pd
from lmdb_utils import *
from tqdm import tqdm
import cv2
from utils import *

class CIFAR10Loader(object):

    def __init__(self, db_path=None, mode="train", img_size=128):
        """
        Initialize the data sampler with training data.
        """
        self.data = lmdb.open(db_path,  max_readers=1, readonly=True, lock=False, readahead=False, meminit=False)
        self.reader = self.data.begin()
        self.N = get_data(self.reader,"num_samples")
        self.mode = mode
        
        # Transforms (Data Augmentation)
        normalize = transforms.Normalize(mean=[0.4914 , 0.48216, 0.44653], std=[0.24703, 0.24349, 0.26159])
        if mode == "train":
            scale_size = 40
            padding = int((scale_size - img_size) / 2)
            self.transform = transforms.Compose([transforms.RandomCrop(img_size, padding=padding),
                    transforms.ColorJitter(.25,.25,.25),
                    transforms.RandomRotation(2),
                    transforms.RandomHorizontalFlip(),
                    transforms.ToTensor(),
                    normalize
            ])
        else:
            self.transform = transforms.Compose([
                    transforms.ToTensor(),
                    normalize
            ])

        self.classes = get_data(self.reader, "classes") 

    def __len__(self):
        """
        Number of images in the object dataset.
        """
        return self.N

    def __getitem__(self, index):
        image = Image.fromarray(get_data(self.reader,"images_"+str(index)))
        label = get_data(self.reader, "label_"+str(index))
        # label = np.array(self.classes[label] != "cat").astype("int64")
        return self.transform(image), label


class ExpDataLoaders(object):
    def __init__(self, train_loader=None, val_loader=None, test_loader=None):
        self.train_loader = train_loader
        self.val_loader = val_loader
        self.test_loader = test_loader

def get_loader(data_path, img_size, batch_size,mode='train'):
    """Build and return data loader."""

    dataset = CIFAR10Loader(data_path, mode, img_size)

    shuffle = False
    if mode == 'train':
        shuffle = True
    
    data_loader = DataLoader(dataset=dataset,
                             batch_size=batch_size,
                             shuffle=shuffle,
                             drop_last=False)
    return data_loader

if __name__ == "__main__":
    db = get_loader("C:/Datasets/cifar10-train-50000", img_size=32, batch_size=1)
    print(len(db))
    for itr, (image, label) in enumerate(tqdm(db)):
        image = image.cuda()
        label = label.cuda()

        print(label)
        print(torch.ones(label.size(0)).cuda())
        break
        # if label == 0:
        #     image = np.transpose(denorm(image).squeeze().data.cpu().numpy(),[1,2,0])
        #     plt.imshow(image)
        #     plt.show()
        #     break
        
        
        
            
