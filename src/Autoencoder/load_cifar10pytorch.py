import pickle 
import numpy as np
import matplotlib.pyplot as plt
import lmdb
from lmdb_utils import *
np.random.seed(31)

def unpickle(file):
    with open(file, 'rb') as fo:
        dict = pickle.load(fo, encoding='bytes')
    return dict

# META_DATA dict_keys([b'num_cases_per_batch', b'label_names', b'num_vis'])
META_DATA = unpickle("./cifar-10-batches-py/batches.meta") 
classes = [x.decode() for x in META_DATA[b"label_names"]]

def parse_cifar(DATA):
	parsed = {}
	for i,(label, data) in enumerate(zip(DATA[b'labels'], DATA[b'data'])):
		label = META_DATA[b"label_names"][label].decode()
		data = np.transpose(data.reshape([3,32,32]),[1,2,0])

		# Check if the specific category already exist
		if label not in parsed:
			parsed[label] = []

		parsed[label].append(data)
	return parsed

# Load all batches
parsed = {}
for batch in range(1, 6):
	batch_data = parse_cifar(unpickle("./cifar-10-batches-py/data_batch_"+str(batch)))

	for key, values in batch_data.items():
		if key not in parsed:
			parsed[key] = values
		else:
			parsed[key] += values


# Show frequencies of each class
N_data = 0 
for k, v in parsed.items():
	print(k, len(v))
	N_data += len(v)

# Save to lmdb
def create_lmdb(prefix, N_data, data):
	dataset_name = "C:/Datasets/cifar10-{}-{}".format(prefix, N_data)
	print("Generating LMDB", dataset_name)
	env = lmdb.open(dataset_name, map_size=1)

	cache = {}
	cache["num_samples"] = pickle.dumps(N_data)
	cache["classes"] = pickle.dumps(classes)
	iterator = 0
	for item in data:
		for i, x in enumerate(data[item]):
			cache["images_"+str(iterator)] = pickle.dumps(data[item][i])
			cache["label_"+str(iterator)] = pickle.dumps(classes.index(item))
			iterator += 1

	if len(cache) > 0:
	    writeCache(env, cache)
	    cache = {}

create_lmdb("train", N_data, parsed)


parsed = parse_cifar(unpickle("./cifar-10-batches-py/test_batch"))

# Show frequencies of each class
N_data = 0 
for k, v in parsed.items():
	print(k, len(v))
	N_data += len(v)

create_lmdb("test", N_data, parsed)