
Epoch [1/100], Iter [10/24],
D/total_loss: 0.2834,
D/total_loss_avg: 0.3167
lr: 0.0002
momentum: 0.9
Elapsed 0:00:08.874282 / Remaining Time: 0:35:21.840741

Epoch [1/100], Iter [20/24],
D/total_loss: 0.2262,
D/total_loss_avg: 0.2801
lr: 0.0002
momentum: 0.9
Elapsed 0:00:16.237154 / Remaining Time: 0:32:13.033185

Epoch [2/100], Iter [10/24],
D/total_loss: 0.1686,
D/total_loss_avg: 0.6464
lr: 0.0002
momentum: 0.9
Elapsed 0:00:30.997116 / Remaining Time: 0:35:57.946271

Epoch [2/100], Iter [20/24],
D/total_loss: 0.1514,
D/total_loss_avg: 0.3780
lr: 0.0002
momentum: 0.9
Elapsed 0:00:38.976467 / Remaining Time: 0:34:47.898478

Epoch [3/100], Iter [10/24],
D/total_loss: 0.1146,
D/total_loss_avg: 0.6870
lr: 0.0002
momentum: 0.9
Elapsed 0:00:53.601665 / Remaining Time: 0:36:05.322453

Epoch [3/100], Iter [20/24],
D/total_loss: 0.0988,
D/total_loss_avg: 0.3668
lr: 0.0002
momentum: 0.9
Elapsed 0:01:00.771657 / Remaining Time: 0:34:45.004063

Epoch [4/100], Iter [10/24],
D/total_loss: 0.0830,
D/total_loss_avg: 0.6219
lr: 0.0002
momentum: 0.9
Elapsed 0:01:14.898297 / Remaining Time: 0:35:18.160384

Epoch [4/100], Iter [20/24],
D/total_loss: 0.0795,
D/total_loss_avg: 0.3239
lr: 0.0002
momentum: 0.9
Elapsed 0:01:22.102403 / Remaining Time: 0:34:20.591830

Epoch [5/100], Iter [10/24],
D/total_loss: 0.0712,
D/total_loss_avg: 0.5439
lr: 0.0002
momentum: 0.9
Elapsed 0:01:37.361901 / Remaining Time: 0:35:07.977008

Epoch [5/100], Iter [20/24],
D/total_loss: 0.0683,
D/total_loss_avg: 0.2827
lr: 0.0002
momentum: 0.9
Elapsed 0:01:45.093919 / Remaining Time: 0:34:30.168999
