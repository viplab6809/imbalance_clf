import os
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import numpy as np
from torch.nn import init
import matplotlib.pyplot as plt

class CosineAnealing():
    def __init__(self, initial_lr, itr_per_epoch, cycle_multiplier):
        self.iteration = 0
        self.initial_lr = initial_lr
        self.itr_per_epoch = itr_per_epoch
        self.cycle_multiplier = cycle_multiplier

    def compute_lr(self):
        if self.iteration < self.itr_per_epoch / 20:
            self.iteration += 1
            return self.initial_lr / 100.0

        cos_out = np.cos(np.pi*self.iteration / self.itr_per_epoch) + 1
        self.iteration += 1
        if self.iteration == self.itr_per_epoch:
            self.iteration = 0
            self.itr_per_epoch *= self.cycle_multiplier

        return self.initial_lr / 2 * cos_out

class LR_Finder():
    def __init__(self, min_lr, max_lr, itr_per_epoch, linear=False):
        self.iteration = 0
        self.linear = linear
        self.min_lr = min_lr
        ratio = max_lr/min_lr
        self.lr_mult = (ratio/itr_per_epoch) if linear else ratio**(1/itr_per_epoch)
        self.lrs = []
        self.losses = []

    def compute_lr(self):
        self.iteration += 1
        mult = self.lr_mult*self.iteration if self.linear else self.lr_mult**self.iteration
        lr = self.min_lr * mult
        self.lrs.append(lr)
        
        return lr

    def plot(self, n_skip=10, n_skip_end=5):
        plt.plot(self.lrs[n_skip:-n_skip_end], self.losses[n_skip:-n_skip_end])
        plt.ylabel("loss")
        plt.xlabel("learning rate (log scale)")
        plt.xscale('log')

class CircularLR_beta():
    def __init__(self, initial_lr, itr_per_epoch, div=10, pct=10, max_mom=0.95, min_mom=0.85):
        # cycle_len: the total length of the cycle (in epochs)
        # lr: the maximum learning rate
        # div: by how much do we want to divide the maximum lr
        # pct: what percentage of epochs should be left at the end for the final annealing
        # max_mom: the maximum momentum
        # min_mom: the minimum momentum

        

        self.initial_lr = initial_lr
        self.itr_per_epoch = itr_per_epoch

        self.div = div
        self.pct = pct
        self.max_mom = max_mom
        self.min_mom = min_mom

        self.iteration = 0
        self.cycle_ctr = 0
        self.cycle_iterations = int(self.itr_per_epoch * (1 - self.pct/100) / 2)

    def compute_lr(self):

        if self.iteration > 2 * self.cycle_iterations:
            pct = (self.iteration - 2 * self.cycle_iterations) / (self.itr_per_epoch - 2 * self.cycle_iterations)
            next_lr = self.initial_lr * (1 + (pct * (1 - 100) / 100)) / self.div
        elif self.iteration > self.cycle_iterations:
            pct = 1 - (self.iteration - self.cycle_iterations) / self.cycle_iterations
            next_lr = self.initial_lr * (1 + pct * (self.div - 1)) / self.div
        else:
            pct = self.iteration / self.cycle_iterations
            next_lr = self.initial_lr * (1 + pct * (self.div-1)) / self.div

        self.iteration += 1

        if self.iteration == self.itr_per_epoch:
            self.iteration = 0
            self.cycle_ctr += 1

        return next_lr


    def compute_momentum(self):

        if self.iteration > 2 * self.cycle_iterations:
            momentum = self.max_mom
        elif self.iteration > self.cycle_iterations:
            pct = 1 - (self.iteration - self.cycle_iterations) / self.cycle_iterations
            momentum = self.max_mom - pct * (self.max_mom - self.min_mom)
        else:
            pct = self.iteration / self.cycle_iterations
            momentum = self.max_mom - pct * (self.max_mom - self.min_mom)

        return momentum