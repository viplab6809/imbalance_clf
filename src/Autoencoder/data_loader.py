import torch
import os
import random
from torch.utils.data import Dataset
from torch.utils.data import DataLoader
from torchvision import transforms
from torchvision.datasets import ImageFolder
import matplotlib.pyplot as plt
from PIL import Image
import h5py
import numpy as np
import collections
import numbers
import math
import pandas as pd
from lmdb_utils import *
from tqdm import tqdm
import cv2
from utils import *

class MiniImageNetLoader(object):

    def __init__(self, db_path=None, mode="train", num_classes=20, img_size=96, random_seed=1):
        """
        Initialize the data sampler with training data.
        """
        self.data = pickle.load(open(db_path,"rb"))
        self.num_classes = num_classes

        np.random.seed(random_seed)
        
        self.chosen_classes = np.random.choice(list(self.data["class_dict"].keys()), num_classes, replace=False)

        self.N = len(self.data["class_dict"][self.chosen_classes[0]])

        self.mode = mode
        
        # Transforms (Data Augmentation)
        normalize = transforms.Normalize(mean=[0.5 , 0.5, 0.5], std=[0.5, 0.5, 0.5])
        if mode == "train":
            self.transform = transforms.Compose([transforms.Resize(img_size),
                    transforms.ColorJitter(.25,.25,.25),
                    transforms.RandomRotation(2),
                    transforms.RandomHorizontalFlip(),
                    transforms.ToTensor(),
                    normalize
            ])
        else:
            self.transform = transforms.Compose([
                    transforms.ToTensor(),
                    normalize
            ])


    def __len__(self):
        """
        Number of images in the object dataset.
        """
        return self.N*self.num_classes

    def __getitem__(self, index):   

        class_idx = index % self.num_classes
        img_idx = index // self.num_classes

        image = self.data["image_data"][self.data["class_dict"][self.chosen_classes[class_idx]][img_idx]]
        image = Image.fromarray(image)

        return self.transform(image), class_idx


class ExpDataLoaders(object):
    def __init__(self, train_loader=None, val_loader=None, test_loader=None):
        self.train_loader = train_loader
        self.val_loader = val_loader
        self.test_loader = test_loader

def get_loader(data_path, img_size, batch_size, num_classes=20, mode='train', random_seed=1):
    """Build and return data loader."""

    dataset = MiniImageNetLoader(data_path, mode, num_classes=num_classes, img_size=img_size, random_seed=random_seed)

    shuffle = False
    if mode == 'train':
        shuffle = True
    
    data_loader = DataLoader(dataset=dataset,
                             batch_size=batch_size,
                             shuffle=shuffle,
                             drop_last=False)
    return data_loader

if __name__ == "__main__":
    db = get_loader("C:/Datasets/mini-imagenet/mini-imagenet-cache-train.pkl", img_size=96, batch_size=16)
    print(len(db))
    for itr, (image, label) in enumerate(tqdm(db)):
        image = image.cuda()
        label = label.cuda()

        print(image.size())
        print(label)
        break
        # if label == 0:
        #     image = np.transpose(denorm(image).squeeze().data.cpu().numpy(),[1,2,0])
        #     plt.imshow(image)
        #     plt.show()
        #     break
        
        
        
            
