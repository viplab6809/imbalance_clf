import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
import torchvision
from torch.autograd import Variable
import itertools
from utils import *
from layers import *
import argparse



def children(m): return m if isinstance(m, (list, tuple)) else list(m.children())

def num_features(m):
    c=children(m)
    if len(c)==0: return None
    for l in reversed(c):
        if hasattr(l, 'num_features'): 
            return l.num_features
        res = num_features(l)
        if res is not None: return res

class AdaptiveConcatPool2d(nn.Module):
    def __init__(self, sz=None, adaptive_pool=["max","avg"]):
        super().__init__()
        sz = sz or (1,1)
        self.ap = nn.AdaptiveAvgPool2d(sz)
        self.mp = nn.AdaptiveMaxPool2d(sz)
        self.adaptive_pool = adaptive_pool

    def forward(self, x): 
        layers = []
        if isinstance(self.adaptive_pool, (list, tuple)):
            for l in self.adaptive_pool:
                if l == "max":
                    layers.append(self.mp(x))
                else:
                    layers.append(self.ap(x))
        else:
            if l == "max":
                layers.append(self.mp(x))
            else:
                layers.append(self.ap(x))

        return torch.cat(layers, 1)

class Lambda(nn.Module):
    def __init__(self, f): super().__init__(); self.f=f
    def forward(self, x): return self.f(x)

class Flatten(nn.Module):
    def __init__(self): super().__init__()
    def forward(self, x): return x.view(x.size(0), -1)


class Autoencoder(nn.Module):
    """docstring for Autoencoder"""
    def __init__(self, n_layers = 4, hidden_size=64, out_size=64):
        super(Autoencoder, self).__init__()
        layers = []
        layers.append(nn.Conv2d(3, hidden_size, kernel_size=3, stride=2, padding=1))
        layers.append(nn.BatchNorm2d(hidden_size))
        layers.append(nn.ReLU())
        # layers.append(nn.MaxPool2d(2))

        for i in range(1,n_layers):
            layers.append(nn.Conv2d(hidden_size, hidden_size, kernel_size=3, stride=2, padding=1))
            layers.append(nn.BatchNorm2d(hidden_size))
            layers.append(nn.ReLU())
            # layers.append(nn.MaxPool2d(2))
        
        self.encoder = nn.Sequential(*layers)

        layers = []

        for i in range(1,n_layers):
            layers.append(nn.ConvTranspose2d(hidden_size, hidden_size, kernel_size=4, stride=2, padding=1))
            layers.append(nn.BatchNorm2d(hidden_size))
            layers.append(nn.ReLU())
            # layers.append(nn.MaxPool2d(2))

        layers.append(nn.ConvTranspose2d(hidden_size, 3, kernel_size=4, stride=2, padding=1))
        layers.append(nn.Tanh())
        
        self.decoder = nn.Sequential(*layers)

    def encode(self, x):
        return self.encoder(x)

    def decode(self, x):
        return self.decoder(x)

    def forward(self, x):
        enc = self.encode(x)
        dec = self.decode(enc)
        return dec

def str2bool(v):
    return v.lower() in ('true')


if __name__ == '__main__':
    model = Autoencoder()

    inp = torch.randn(16,3,96,96)

    out = model(inp)
    print(out.size())