import os
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import numpy as np
from scipy.stats import norm
import cv2
from torch.nn import init
import cv2

def to_var(x, requires_grad=False, fp16=False):
    if torch.cuda.is_available():
        x = x.cuda()
        if fp16:
            x = x.half()
    x.requires_grad_(requires_grad)
    return x

def to_bgr(image):
    (r,g,b) = cv2.split(image)
    return cv2.merge([b,g,r])
def to_rgb(image):
    (b,g,r) = cv2.split(image)
    return cv2.merge([r,g,b])
def show_image(image, title="Image"):
    (r,g,b) = cv2.split(image)
    img = cv2.merge([b,g,r])
    cv2.imshow(title,img)
    cv2.waitKey()
    cv2.destroyAllWindows()

def denorm(x, clamp=True):
    out = (x + 1) / 2
    if not clamp:
        return out
    return out.clamp_(0, 1)

def to_numpy(x):
    if x.is_cuda:
        return np.squeeze(x.cpu().data.numpy())
    return np.squeeze(x.data.numpy())

def to_image(x):
    return np.transpose(to_numpy(denorm(x)),[1,2,0])

def init_weights(net, init_type='normal'):
    def init_func(m):
        classname = m.__class__.__name__
        if hasattr(m, 'weight') and (classname.find('Conv') != -1 or classname.find('Linear') != -1):
            if init_type == 'normal':
                init.normal(m.weight.data, 0.0, 0.02)
            elif init_type == 'xavier':
                init.xavier_normal(m.weight.data, gain=0.02)
            elif init_type == 'kaiming':
                init.kaiming_normal(m.weight.data, a=0, mode='fan_in')
            elif init_type == 'orthogonal':
                init.orthogonal(m.weight.data, gain=1)
            else:
                raise NotImplementedError('initialization method [%s] is not implemented' % init_type)
        elif classname.find('BatchNorm2d') != -1:
            init.normal(m.weight.data, 1.0, 0.02)
            init.constant(m.bias.data, 0.0)
    return init_func


def init_net(net, init_type='normal', gpu_ids=[]):
    if len(gpu_ids) > 0:
        assert(torch.cuda.is_available())
        net.cuda(gpu_ids[0])
        net = torch.nn.DataParallel(net, gpu_ids)
    init_weights(net, init_type=init_type)
    net.apply(init_weights(init_type))
    return net
    
def get_intersection(A, B):
    dx = min(A[2], B[2]) - max(A[0], B[0])
    dy = min(A[3], B[3]) - max(A[1], B[1])
    if (dx>=0) and (dy>=0):
        return dx*dy
    return 0

def get_area(A):
    # return width * height
    return (A[2]-A[0]) * (A[3]-A[1])

def compute_average_precision(target, output, IOU_TH = 0.1):
    (TP, FN, FP) = (0,1,2)
    f = [0,0,0]

    # Compute the intersection over union 
    list_of_t = []
    list_of_o = []
    for i,t in enumerate(target):
        for j,o in enumerate(output):
            if o in list_of_o: continue
            if t in list_of_t: continue    
            target_area = get_area(t)
            output_area = get_area(o)
            
            intersection = get_intersection(t,o)
            union = target_area + output_area - intersection
            

            if (intersection / union) > IOU_TH:
                f[TP] += 1
                list_of_t.append(t)        
                list_of_o.append(o)
            
                
            # print(i,j,"|",target_area, output_area,intersection,"|",f)
    f[FP] += len(output) - len(list_of_t)
    f[FN] += len(target) - len(list_of_t)

    return f


def mkdir(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)

def compute_slope(y):
    N = len(y)
    x = np.stack((np.arange(N),np.ones((N,))),axis = 1)
    w = np.linalg.pinv(x.T.dot(x)).dot(x.T).dot(y)
    return np.squeeze(w[0])

def prob_slope(y, value):
    slope = compute_slope(y)
    var = np.var(y,ddof=1)
    N = len(y)
    p_slope = norm(slope,np.sqrt(12*var/(N**3 - N))).cdf(value)
    
    return p_slope

def count_steps_without_decrease(y):
    steps_without_decrease = 0
    n = len(y)
    for i in reversed(range(0,n-1,10)):
        if prob_slope(y[i:n],0) < 0.51:
            steps_without_decrease = n-i
    return steps_without_decrease
    
def count_steps_without_decrease_robust(y):
    p = np.percentile(y,90)
    
    return count_steps_without_decrease(y[y<p])
    
def bgr_to_rgb(image):
    (b,g,r) = cv2.split(image) # get b,g,r
    rgb_img = cv2.merge([r,g,b]) # switch it to rgb
    return rgb_img


class tofp16(nn.Module):
    def __init__(self):
        super(tofp16, self).__init__()

    def forward(self, input):
        return input.half()

def copy_in_params(net, params):
    net_params = list(net.parameters())
    for i in range(len(params)):
        net_params[i].data.copy_(params[i].data)


def set_grad(params, params_with_grad):

    for param, param_w_grad in zip(params, params_with_grad):
        if param.grad is None:
            param.grad = torch.nn.Parameter(param.data.new().resize_(*param.data.size()))
        param.grad.data.copy_(param_w_grad.grad.data)

#BatchNorm layers to have parameters in single precision.
#Find all layers and convert them back to float. This can't
#be done with built in .apply as that function will apply
#fn to all modules, parameters, and buffers. Thus we wouldn't
#be able to guard the float conversion based on the module type.
def BN_convert_float(module):
    if isinstance(module, torch.nn.modules.batchnorm._BatchNorm):
        module.float()
    for child in module.children():
        BN_convert_float(child)
    return module

def to_half(network):
    return nn.Sequential(tofp16(), BN_convert_float(network.cuda().half()))