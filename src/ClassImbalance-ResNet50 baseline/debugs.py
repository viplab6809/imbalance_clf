import torch
import torch.nn as nn
from model import *
from data_loader import *
import matplotlib.pyplot as plt
import itertools
import torch.nn.functional as F
from scipy.stats import norm
import h5py
from utils import *
import torchvision


a = torch.randn(5,20,1,1)
a = a.expand((5,20,5,5))
print(a.size())