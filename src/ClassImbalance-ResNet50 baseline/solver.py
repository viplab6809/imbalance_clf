import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
import os
import time
import datetime
from torch.autograd import grad
from torch.autograd import Variable
from torchvision.utils import save_image
from torchvision import transforms
from model import *
from PIL import Image, ImageDraw, ImageFont
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from utils import *
from data_loader import *
from tqdm import tqdm
from lr_schedulers import *
from fp16 import *

class Solver(object):
    DEFAULTS = {}   
    def __init__(self, config, data_loader):
        # Data loader
        self.__dict__.update(Solver.DEFAULTS, **config)
        self.data_loader = data_loader

        # Build tensorboard if use
        self.build_model(config)
        if self.use_tensorboard:
            self.build_tensorboard()

        # Start with trained model
        if self.pretrained_model:
            self.load_pretrained_model()
        
    def build_model(self, config):
        # Define a model
        self.model = DefaultClassifiers(architecture="preact_resnet50", n_output=self.n_output, n_cls_layers = 1, adaptive_pool=["max"])
         
        if self.freeze_layers:
            self.model.freeze()

        # Optimizers
        
        if self.freeze_layers:
            # only last layer
            self.optimizer = torch.optim.SGD(self.model.get_trainable_params(), lr=self.lr, momentum=0.9, weight_decay=self.weight_decay)
        else:
            self.optimizer = torch.optim.SGD(self.model.get_trainable_params(), lr=self.lr, momentum=self.max_momentum, weight_decay=self.weight_decay)
            # self.optimizer = torch.optim.SGD(self.model.create_param_groups(self.model.layer_groups,lr_list=[self.lr/9,self.lr/3, self.lr]), lr=self.lr, momentum=0.9, weight_decay=self.lambda_reg)


        # Print networks
        self.print_network(self.model, 'CIFAR10 Classifier')

        if torch.cuda.is_available():
            self.model.cuda()
            if self.fp16:
                self.model = FP16(self.model)
                self.model_fp32 = copy_model_to_fp32(self.model, self.optimizer)

    def print_network(self, model, name):
        num_params = 0
        for p in model.parameters():
            num_params += p.numel()
        print(name)
        print(model)
        print("The number of parameters: {}".format(num_params))

    def load_pretrained_model(self):
        self.model.load_state_dict(torch.load(os.path.join(
            self.model_save_path, '{}_model.pth'.format(self.pretrained_model))))

        print('loaded trained models (step: {})..!'.format(self.pretrained_model))

    def save_model(self,e,i):
        torch.save(self.model.state_dict(),
            os.path.join(self.model_save_path, '{}_{}_model.pth'.format(e, i)))

        print('Model Checkpoint (step: {} - {})..!'.format(e, i))

    def reset_grad(self):
        self.model.zero_grad()
        self.optimizer.zero_grad()

    def log_progress(self,e,i, loss, start_time,iters_per_epoch):
        elapsed = time.time() - start_time
        total_time = ((self.num_epochs*iters_per_epoch)-(e*iters_per_epoch+i)) * elapsed/(e*iters_per_epoch+i+1)
        epoch_time = (iters_per_epoch-i)* elapsed/(e*iters_per_epoch+i+1)
        
        epoch_time = str(datetime.timedelta(seconds=epoch_time))
        total_time = str(datetime.timedelta(seconds=total_time))
        elapsed = str(datetime.timedelta(seconds=elapsed))

        # lr_tmp = []
        # for param_group in self.gen_optimizer.param_groups:
        #     lr_tmp.append(param_group['lr'])
        # tmplr = np.squeeze(np.array(lr_tmp))

        log = "\nEpoch [{}/{}], Iter [{}/{}]".format(e+1, self.num_epochs, i+1, iters_per_epoch)

        for tag, value in sorted(loss.items()):
            log += ",\n{}: {:.4f}".format(tag, value)

        lr_tmp = []
        mom_tmp = []
        for param_group in self.optimizer.param_groups:
            lr_tmp.append(param_group['lr'])
            if 'betas' in param_group:
                mom_tmp.append(param_group['betas'][0])
            else:
                mom_tmp.append(param_group['momentum'])

        tmplr = np.squeeze(np.array(lr_tmp))
        mom_tmp = np.squeeze(np.array(mom_tmp))
        log += "\nlr: {}".format(tmplr)
        log += "\nmomentum: {}".format(mom_tmp)
        log += "\nElapsed {} / Remaining Time: {}\n".format(elapsed,total_time)
        print(log)
        with open(self.log_path+"/train_log-{}.txt".format(self.train_timestart.strftime("%Y-%m-%d %H-%M")),"a") as f:
            f.write(log)

        if self.use_tensorboard:
            for tag, value in loss.items():
                self.logger.scalar_summary(tag, value, e * iters_per_epoch + i + 1)

    def build_tensorboard(self):
        from logger import Logger
        self.logger = Logger(self.log_path)

    def update_lr(self, optimizer, lr):
        if not isinstance(lr, (list, tuple)):
            lr = [lr] * len(optimizer.param_groups)

        assert len(optimizer.param_groups) == len(lr), f'size mismatch, expected lengths {len(optimizer.param_groups)}, but got {len(optimizer.param_groups)} and {len(lr)} instead.'

        for i,param_group in enumerate(optimizer.param_groups):
            param_group['lr'] = lr[i]

    def update_momentum(self, optimizer, momentum):
        if not isinstance(momentum, (list, tuple)):
            momentum = [momentum] * len(optimizer.param_groups)
        assert len(optimizer.param_groups) == len(momentum), f'size mismatch, expected lengths {len(optimizer.param_groups)}, but got {len(optimizer.param_groups)} and {len(momentum)} instead.'
        if 'betas' in optimizer.param_groups[0]:
            for i, pg in enumerate(optimizer.param_groups): pg['betas'] = (momentum[i], pg['betas'][1])
        else:
            for i, pg in enumerate(optimizer.param_groups): pg['momentum'] = momentum[i]

    def lr_scheduler(self, lr_sched, optimizer):
        new_lr = lr_sched.compute_lr()
        new_momentum = lr_sched.compute_momentum()
        if self.freeze_layers:
            self.update_lr(optimizer,new_lr)
        else:
            self.update_lr(optimizer,new_lr)
            self.update_momentum(optimizer, new_momentum)
            # self.update_lr([new_lr/9,new_lr/3,new_lr])

    def lr_finder(self, min_lr=1e-5, max_lr=10):
        losses = []
        iters_per_epoch = len(self.data_loader.train_loader)

        lr_sched = LR_Finder(min_lr, max_lr, iters_per_epoch, linear=False)
        
        best = 1e9
        avg_mom = 0.98
        avg_loss = 0

        for i, (image, label) in enumerate(tqdm(self.data_loader.train_loader)):
            image = to_var(image)
            label = to_var(label) 

            self.lr_scheduler(lr_sched, self.optimizer)

            total_loss = self.model_step(image, label)

            loss = total_loss
            avg_loss = avg_loss * avg_mom + loss * (1-avg_mom)
            debias_loss = avg_loss / (1 - avg_mom**(i+1))


            lr_sched.losses.append(debias_loss)

            if np.isnan(debias_loss) or debias_loss > best * 4:
                break

            if (debias_loss < best and i > 10): 
                best = debias_loss

        plt.subplot(121)    
        lr_sched.plot()
        plt.subplot(122)
        plt.plot(lr_sched.lrs)
        plt.show()


    def train(self):
        avg_mom = 0.98
        avg_loss = 0

        iters_per_epoch = len(self.data_loader.train_loader)
        # lr cache for decaying
        self.curr_lr = self.lr
        self.lr_sched = CircularLR_beta(self.lr, iters_per_epoch*self.num_epochs, div=self.lr_div, pct=self.pct, max_mom=self.max_momentum, min_mom=self.min_momentum)

        # Start with trained model if exists
        if self.pretrained_model:
            start_e = int(self.pretrained_model.split('_')[0])-1
        else:
            start_e = 0

        # Start training
        start_time = time.time()        
        loss = {}

        self.train_timestart = datetime.datetime.now()

        for e in range(start_e, self.num_epochs):
            for i, (image, labels) in enumerate(tqdm(self.data_loader.train_loader)):
                self.lr_scheduler(self.lr_sched, self.optimizer)

                image = to_var(image)
                labels = to_var(labels)

                total_loss = self.model_step(image, labels)
                avg_loss = avg_loss * avg_mom + total_loss * (1-avg_mom)
                debias_loss = avg_loss / (1 - avg_mom**(i+1))

                # Logging
                loss['D/total_loss'] = total_loss
                loss['D/total_loss_avg'] = debias_loss

                # Print out log info
                if (i+1) % self.log_step == 0:
                    self.log_progress(e, i, loss, start_time, iters_per_epoch)

                # Save model checkpoints
                if (i+1) % self.model_save_step == 0:
                    self.save_model(e+1, i+1)   


                
            # save every epoch
            self.save_model(e+1, i+1)

            val_loss, val_acc = self.evaluate_model(self.data_loader.val_loader)
            log = "Epoch: {}\tTrain Loss: {:0.4f}\tVal loss: {:0.4f}\tVal Accuracy: {:0.4f}\n".format(e, debias_loss, val_loss, val_acc)
            print(log)
            with open(self.log_path+"/val_log-{}.txt".format(self.train_timestart.strftime("%Y-%m-%d %H-%M")),"a") as f:
                f.write(log)

    def model_step(self, image, labels):
        self.model.train()
        
        n = image.size(0)
        loss = 0
        for i in range(self.batch_accumulation):
            fromIdx = i * (n // self.batch_accumulation)
            toIdx = (i + 1) * (n // self.batch_accumulation)

            image_batch = image[fromIdx:toIdx]
            labels_batch = labels[fromIdx:toIdx]

            predictions = self.model(image_batch).squeeze()
            
            total_loss = F.cross_entropy(predictions, labels_batch) / self.batch_accumulation
            
            loss += total_loss.data.item() 

            if self.fp16:
                total_loss = self.loss_scale * total_loss
            
            if i == 0:
                self.reset_grad()

            total_loss.backward()

            if i == self.batch_accumulation - 1:
                if self.fp16: 
                    update_fp32_grads(self.model_fp32, self.model)
                    if self.loss_scale != 1:
                        for param in self.model_fp32: param.grad.data.div_(self.loss_scale)

                self.optimizer.step()

                if self.fp16: 
                    copy_fp32_to_model(self.model, self.model_fp32)
                    torch.cuda.synchronize()
        return loss
    # def model_step(self, image, labels):
    #     self.model.train()
        
    #     predictions = self.model(image).squeeze()
        
    #     total_loss = F.cross_entropy(predictions, labels) 
        
    #     loss = total_loss.data.item()

    #     if self.fp16:
    #         total_loss = self.loss_scale * total_loss

    #     self.reset_grad()
    #     total_loss.backward()
        
    #     if self.fp16: 
    #         update_fp32_grads(self.model_fp32, self.model)
    #         if self.loss_scale != 1:
    #             for param in self.model_fp32: param.grad.data.div_(self.loss_scale)

    #     self.optimizer.step()

    #     if self.fp16: 
    #         copy_fp32_to_model(self.model, self.model_fp32)
    #         torch.cuda.synchronize()
    #     return loss
    def evaluate_model(self,data_loader):
        self.model.eval()
        print("Computing performance on Validation Set")
        losses = []
        acc_list = []
        with torch.no_grad():
            size = 0
            for i, (image, labels) in enumerate(tqdm(data_loader)):
                image = to_var(image)
                labels = to_var(labels)
                predictions = self.model(image).squeeze()

                loss = F.cross_entropy(predictions, labels) 
                losses.append(loss.data.item())

                pred_labels = torch.max(predictions, dim=1)[1].long()

                accuracy = (pred_labels==labels).float().sum()
                size += pred_labels.size(0)
                acc_list.append(accuracy.data.item())
        return np.mean(losses), np.sum(acc_list) / size