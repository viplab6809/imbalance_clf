# import lmdb
import numpy as np
import cv2
from os import listdir
import lmdb
import pickle
from tqdm import tqdm
import pickle
import lmdb

def add_sample(db, key, value):    
    """
    Write (key,value) to db
    """
    success = False
    while not success:
        txn = db.begin(write=True)
        try:
            txn.put(key, value)
            txn.commit()
            success = True
            curr_limit = db.info()['map_size']
        except lmdb.MapFullError:
            txn.abort()

            # double the map_size
            curr_limit = db.info()['map_size']
            new_limit = curr_limit+128
            db.set_mapsize(new_limit) # Increase the map size
        except:
        	txn.abort()

def get_data(reader, key):
    return pickle.loads(reader.get(key.encode("ascii")))

def writeCache(env, cache):
    print("\nWriting cache")
    # with env.begin(write=True) as txn:
    for k, v in tqdm(cache.items()):
        if not isinstance(v,bytes):
            v = v.encode('ascii')
        if not isinstance(k,bytes):
            k = k.encode('ascii')
       	add_sample(env, k, v)
        

		

	