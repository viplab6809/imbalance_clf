import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
import torchvision
from torch.autograd import Variable
import itertools
from utils import *

def l2normalize(v, eps=1e-12):
    return v / (v.norm() + eps)
    
class Inception(nn.Module):
    """Residual Block with instance normalization."""
    def __init__(self, dim_in, dim_out):
        super(Inception, self).__init__()
        self.conv1 = nn.Conv2d(dim_in, dim_out, kernel_size=1, stride=1, padding=0, bias=False)
        self.conv3 = nn.Conv2d(dim_in, dim_out, kernel_size=3, stride=1, padding=1, bias=False)
        self.conv5 = nn.Conv2d(dim_in, dim_out, kernel_size=5, stride=1, padding=2, bias=False)

    def forward(self, x):
        return torch.cat([self.conv1(x),self.conv3(x),self.conv5(x)],1)

class ResidualBlock(nn.Module):
    """Residual Block with instance normalization."""
    def __init__(self, dim_in, dim_out):
        super(ResidualBlock, self).__init__()
        self.main = nn.Sequential(
            nn.Conv2d(dim_in, dim_out, kernel_size=3, stride=1, padding=1, bias=False),
            nn.InstanceNorm2d(dim_out, affine=True, track_running_stats=True),
            nn.ReLU(inplace=True),
            nn.Conv2d(dim_out, dim_out, kernel_size=3, stride=1, padding=1, bias=False),
            nn.InstanceNorm2d(dim_out, affine=True, track_running_stats=True))

    def forward(self, x):
        return x + self.main(x)

# https://github.com/christiancosgrove/pytorch-spectral-normalization-gan
class SpectralNorm(nn.Module):
    def __init__(self, module, name='weight', power_iterations=1):
        super(SpectralNorm, self).__init__()
        self.module = module
        self.name = name
        self.power_iterations = power_iterations
        if not self._made_params():
            self._make_params()

    def _update_u_v(self):
        u = getattr(self.module, self.name + "_u")
        v = getattr(self.module, self.name + "_v")
        w = getattr(self.module, self.name + "_bar")

        height = w.data.shape[0]
        for _ in range(self.power_iterations):
            v.data = l2normalize(torch.mv(torch.t(w.view(height,-1).data), u.data))
            u.data = l2normalize(torch.mv(w.view(height,-1).data, v.data))

        # sigma = torch.dot(u.data, torch.mv(w.view(height,-1).data, v.data))
        sigma = u.dot(w.view(height, -1).mv(v))
        setattr(self.module, self.name, w / sigma.expand_as(w))

    def _made_params(self):
        try:
            u = getattr(self.module, self.name + "_u")
            v = getattr(self.module, self.name + "_v")
            w = getattr(self.module, self.name + "_bar")
            return True
        except AttributeError:
            return False


    def _make_params(self):
        w = getattr(self.module, self.name)

        height = w.data.shape[0]
        width = w.view(height, -1).data.shape[1]

        u = nn.Parameter(w.data.new(height).normal_(0, 1), requires_grad=False)
        v = nn.Parameter(w.data.new(width).normal_(0, 1), requires_grad=False)
        u.data = l2normalize(u.data)
        v.data = l2normalize(v.data)
        w_bar = nn.Parameter(w.data)

        del self.module._parameters[self.name]

        self.module.register_parameter(self.name + "_u", u)
        self.module.register_parameter(self.name + "_v", v)
        self.module.register_parameter(self.name + "_bar", w_bar)


    def forward(self, *args):
        self._update_u_v()
        return self.module.forward(*args)


# https://github.com/heykeetae/Self-Attention-GAN
class SelfAttention(nn.Module):
    """ Self attention Layer"""
    def __init__(self, in_dim):
        super(SelfAttention,self).__init__()
        self.chanel_in = in_dim

        self.f = nn.Conv2d(in_channels = in_dim , out_channels = in_dim//8 , kernel_size= 1)
        self.g = nn.Conv2d(in_channels = in_dim , out_channels = in_dim//8 , kernel_size= 1)
        self.h = nn.Conv2d(in_channels = in_dim , out_channels = in_dim , kernel_size= 1)

        # scale parameter for attention
        self.gamma = nn.Parameter(torch.zeros(1))

        self.softmax  = nn.Softmax(dim=-1)

    def forward(self,x):
        """
            inputs :
                x : input feature maps( B X C X W X H)
            returns :
                out : self attention value + input feature 
                attention: B X N X N (N is Width*Height)
        """
        N, C, H, W = x.size()
        f  = self.f(x).view(N, -1, H*W).permute(0,2,1) # N x (HW) x C 
        g =  self.g(x).view(N, -1, H*W) # N x C x (HW)
        
        s =  torch.bmm(f, g) # transpose check

        # attention (each row sums to 1)
        attention = self.softmax(s) # N x (HW) x (HW) 

        h = self.h(x).view(N, -1, H*W) # N x C x (HW)

        out = torch.bmm(h, attention.permute(0,2,1))
        out = out.view(N, C, H, W)
        
        out = self.gamma*out + x

        return out,attention


class _ConditionalBatchNorm(nn.Module):

    def __init__(self, num_features, label_size, emb_size=128, eps=1e-5, momentum=0.1, affine=True,
                 track_running_stats=True):
        super(_ConditionalBatchNorm, self).__init__()
        self.num_features = num_features
        self.eps = eps
        self.momentum = momentum
        self.affine = affine
        self.track_running_stats = track_running_stats

        if self.affine:
            self.weight = nn.Parameter(torch.Tensor(num_features))
            self.bias = nn.Parameter(torch.Tensor(num_features))
        else:
            self.register_parameter('weight', None)
            self.register_parameter('bias', None)
        if self.track_running_stats:
            self.register_buffer('running_mean', torch.zeros(num_features))
            self.register_buffer('running_var', torch.ones(num_features))
        else:
            self.register_parameter('running_mean', None)
            self.register_parameter('running_var', None)

        # MLP used to predict betas and gammas
        self.fc_gamma = nn.Sequential(
                nn.Linear(label_size, emb_size),
                nn.ReLU(inplace=True),
                nn.Linear(emb_size, num_features),
            )

        self.fc_beta = nn.Sequential(
                nn.Linear(label_size, emb_size),
                nn.ReLU(inplace=True),
                nn.Linear(emb_size, num_features),
            )

        # initialize weights using Xavier initialization and biases with constant value
        for m in self.modules():
            if isinstance(m, nn.Linear):
                nn.init.xavier_uniform_(m.weight)
                nn.init.constant_(m.bias, 0.1)

        self.reset_parameters()

    def reset_parameters(self):
        if self.track_running_stats:
            self.running_mean.zero_()
            self.running_var.fill_(1)
        if self.affine:
            self.weight.data.uniform_()
            self.bias.data.zero_()

    def _check_input_dim(self, input):
        return NotImplemented

    def forward(self, input, label=None):
        self._check_input_dim(input)
        N, C, H, W = input.size()

        gamma = self.weight
        beta = self.bias

        if label is not None:
            delta_gamma = self.fc_gamma(label) # N x C
            delta_beta = self.fc_beta(label) # N x C
        
            gamma = gamma.view(1, self.num_features) + delta_gamma
            beta = beta.view(1, self.num_features) + delta_beta

        normed = F.batch_norm(input, self.running_mean, self.running_var, to_var(torch.ones(self.weight.size())), to_var(torch.zeros(self.bias.size())),
            self.training or not self.track_running_stats, self.momentum, self.eps)

        scaled = gamma.view(-1, self.num_features,1,1) * normed + beta.view(-1, self.num_features,1,1)

        return scaled

    def extra_repr(self):
        return '{num_features}, eps={eps}, momentum={momentum}, affine={affine}, ' \
               'track_running_stats={track_running_stats}'.format(**self.__dict__)

class ConditionalBatchNorm2d(_ConditionalBatchNorm):
    def _check_input_dim(self, input):
        if input.dim() != 4:
            raise ValueError('expected 4D input (got {}D input)'
                             .format(input.dim()))

def get_normalization(normalizer, dim_in, label_size=None, emb_size=None):
    if normalizer == "instance":
        return nn.InstanceNorm2d(dim_in, affine=True, track_running_stats=True)
    elif normalizer == "conditional_batch":
        return ConditionalBatchNorm2d(dim_in, label_size, emb_size)
    return nn.BatchNorm2d(dim_in, affine=True)

class Conv2d(nn.Module):
    """docstring for CoordConv"""
    def __init__(self, dim_in, dim_out, kernel_size, stride, padding, bias=True, with_coord=False, spectral_norm=False):
        super(Conv2d, self).__init__()
        self.with_coord = with_coord

        coord_dim = 2
        if not with_coord:
            coord_dim = 0

        conv = nn.Conv2d(dim_in+coord_dim, dim_out, kernel_size=kernel_size, stride=stride, padding=padding, bias=bias)
        
        if spectral_norm:
            self.main = SpectralNorm(conv)
        else:
            self.main = conv
    def forward(self, x):
        if self.with_coord:
            N,C,H,W = x.size()
            # Append coordinate layer
            coordX = to_var(torch.arange(W).view(1,1,1,W).repeat(N,1,H,1))
            coordY = to_var(torch.arange(W).view(1,1,H,1).repeat(N,1,1,W))
            
            x = torch.cat([x, coordX, coordY],dim = 1)
        
        return self.main(x)


class ConvTranspose2d(nn.Module):
    """docstring for CoordConv"""
    def __init__(self, dim_in, dim_out, kernel_size, stride, padding, bias=True, with_coord=False, spectral_norm=False):
        super(ConvTranspose2d, self).__init__()
        self.with_coord = with_coord

        coord_dim = 2
        if not with_coord:
            coord_dim = 0

        conv = nn.ConvTranspose2d(dim_in+coord_dim, dim_out, kernel_size=kernel_size, stride=stride, padding=padding, bias=bias)
        
        if spectral_norm:
            self.main = SpectralNorm(conv)
        else:
            self.main = conv

    def forward(self, x):
        if self.with_coord:
            N,C,H,W = x.size()
            # Append coordinate layer
            coordX = to_var(torch.arange(W).view(1,1,1,W).repeat(N,1,H,1))
            coordY = to_var(torch.arange(W).view(1,1,H,1).repeat(N,1,1,W))
            
            x = torch.cat([x, coordX, coordY],dim = 1)
        
        return self.main(x)


class ResidualBlockBottleneck(nn.Module):
    """Residual Block."""
    def __init__(self, dim_in, dim_out, normalizer="batch", activation="leakyrelu", with_coord=False, spectral_norm=False):
        super(ResidualBlockBottleneck, self).__init__()
        self.dim_in = dim_in
        self.dim_out = dim_out

        layers = []
        if normalizer is not None:
            layers.append(get_normalization(normalizer, dim_in))            

        if activation is not None:
            layers.append(self.get_activation(activation, 0.2, inplace=False))  

        layers.append(Conv2d(dim_in, dim_out, kernel_size=1, stride=1, padding=0, bias=False, with_coord=with_coord, spectral_norm=spectral_norm))

        if normalizer is not None:
            layers.append(get_normalization(normalizer, dim_out))            

        if activation is not None:
            layers.append(self.get_activation(activation, 0.2))  

        layers.append(Conv2d(dim_out, dim_out, kernel_size=3, stride=1, padding=1, bias=True, with_coord=with_coord, spectral_norm=spectral_norm))

        if normalizer is not None:
            layers.append(get_normalization(normalizer, dim_out))            

        if activation is not None:
            layers.append(self.get_activation(activation, 0.2))  

        layers.append(Conv2d(dim_out, dim_out, kernel_size=3, stride=1, padding=1, bias=True, with_coord=with_coord, spectral_norm=spectral_norm))

        self.residual = nn.Sequential(*layers)

        self.shortcut_conv = Conv2d(self.dim_in, self.dim_out, kernel_size=1, stride=1, padding=0, bias=True, with_coord=with_coord, spectral_norm=spectral_norm)

    def get_activation(self, activation, alpha=0.2, inplace=True):
        if activation == "leakyrelu":
            return nn.LeakyReLU(alpha,inplace=inplace)
        return nn.ReLU(inplace=inplace)

    def shortcut(self, x):
        if self.dim_in != self.dim_out:
            return self.shortcut_conv(x)
        else:
            return x

    def forward(self, x):
        return self.shortcut(x) + self.residual(x)


# class ResidualBlock(nn.Module):
#     """Residual Block."""
#     def __init__(self, dim_in, dim_out, downsample=False, upsample=False, normalizer="batch", label_size=17, emb_size=128,
#                     activation="leakyrelu", with_coord=False, spectral_norm=False):
#         super(ResidualBlock, self).__init__()
#         self.downsample = downsample
#         self.upsample = upsample

#         self.dim_in = dim_in
#         self.dim_out = dim_out

#         layers = []

#         if normalizer is not None:
#             layers.append(get_normalization(normalizer, dim_in, label_size, emb_size))            

#         if activation is not None:
#             layers.append(self.get_activation(activation, 0.2, inplace=False))    

#         if self.upsample:
#             layers.append(nn.Upsample(scale_factor=2, mode='nearest'))

#         layers.append(Conv2d(dim_in, dim_out, kernel_size=3, stride=1, padding=1, bias=False, with_coord=with_coord, spectral_norm=spectral_norm))

#         if normalizer is not None:
#             layers.append(get_normalization(normalizer, dim_out, label_size, emb_size))            
            
#         if activation is not None:
#             layers.append(self.get_activation(activation, 0.2))    


#         if self.downsample:
#             layers.append(nn.AvgPool2d(kernel_size=2,stride=2))

#         layers.append(Conv2d(dim_out, dim_out, kernel_size=3, stride=1, padding=1, bias=True, with_coord=with_coord, spectral_norm=spectral_norm))

#         self.residual = nn.Sequential(*layers)
#         # self.residual = layers

#         layers = []
#         if self.downsample:
#             layers.append(nn.AvgPool2d(kernel_size=2,stride=2))

#         if self.upsample:
#             layers.append(nn.Upsample(scale_factor=2, mode='nearest'))

#         layers.append(Conv2d(dim_in, dim_out, kernel_size=1, stride=1, padding=0, bias=True, with_coord=with_coord, spectral_norm=spectral_norm))

#         self.shortcut_conv = nn.Sequential(*layers)

#     def get_activation(self, activation, alpha=0.2, inplace=True):
#         if activation == "leakyrelu":
#             return nn.LeakyReLU(alpha,inplace=inplace)
#         return nn.ReLU(inplace=inplace)

#     def shortcut(self, x):
#         if self.dim_in != self.dim_out or self.downsample or self.upsample:
#             return self.shortcut_conv(x)
#         else:
#             return x

#     def forward(self, x, labels=None):
#         residual = x
#         for l in self.residual:
#             if isinstance(l, ConditionalBatchNorm2d):
#                 residual = l(residual, labels)
#             else:
#                 residual = l(residual)

#         return self.shortcut(x) + residual
