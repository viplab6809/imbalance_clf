import os
from os import listdir
import argparse
from datetime import datetime
import zipfile
from shutil import *
import time
import threading
import subprocess
import glob
import pandas as pd
import pprint
import numpy as np
import smtplib
import cv2

def find_files(fname):
    return glob.glob(fname)

def mkdir(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)

def parse_folders(args):
    root_src = args["src_folder"]

    if root_src[-1] == "/": 
        root_src=root_src[:-1]
    if args["exp_folder"] == "src_folder": 
        root_exp = root_src+"_exp"
    else: root_exp = args["exp_folder"]

    return root_src+"/", root_exp +"/"

def update_exp(root_src, root_exp, args):
    experiments = pd.read_csv("experiment_list.csv")
    experiments["pc"] = experiments["pc"].str.lower()

    experiments = experiments[experiments["pc"]==args["pc_id"].lower()]
    src_files = list(experiments["exp_name"])

    finished_exp = load_finished_exp(args)

    exp_files = listdir(root_exp)
    new_files = [f for f in src_files if f not in finished_exp]
    new_files = [f for f in src_files if f not in exp_files]

    if len(new_files) > 0:
        print("[{}]Copying required files:({})".format(args["pc_id"]," ; ".join(new_files)))
        for f in new_files:
            try:
                source_dir = "{}/{}/".format(root_src, f)
                target_dir = "{}/{}/".format(root_exp, f)
                copytree(source_dir, target_dir)
            except Exception as e:
                print(e)
                print(source_dir, target_dir)

def run_exp(root_exp, args):
    finished_exp = load_finished_exp(args)
    finished_exp = [exp.strip() for exp in finished_exp]
    list_exp = listdir(root_exp)

    run_exp = [exp for exp in list_exp if exp not in finished_exp]
    for exp in run_exp:
        dir_path = os.path.dirname(os.path.realpath(__file__))

        command = "cd {}/{}/{}/ & run.bat".format(dir_path,root_exp,exp)

        print("Running",command)
        p = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, bufsize=1)
        for line in iter(p.stdout.readline, b''):
            print(line.decode("utf-8"))
        p.wait() # Without this p.returncode will be None
        
        success = p.returncode == 0
        with open("finished_exp.txt","a") as f:
            f.write("{} {}\n".format(exp, success))    

        if args["send_email"]:
            print("===Send email===")
            content = "Subject: {}-{}\n\n[{}--{}] finished running (Success {})".format(exp, success, args["pc_id"], exp, success)
            send_email(content,args)

    return len(run_exp)

def main(args):
    root_src, root_exp = parse_folders(args)

    # Create folders if not exist
    mkdir(root_src)
    while True:
        try:
            mkdir(root_exp)

            print("\n=============================================================")
            print("[{}] Refreshing list {}".format(args["pc_id"], datetime.now()))
            print("=============================================================")

            git_pull(args)
            update_exp(root_src, root_exp, args)
            run_exp(root_exp, args)

            time.sleep(5)
        except Exception as e:
            print(e)


def load_pc_id(args):
    if args["pc_id"] != "1080Ti":
        np.save("pc_id.npy", args["pc_id"])
    elif os.path.exists("pc_id.npy"):
        args["pc_id"] = str(np.load("pc_id.npy"))
    else:
        pass # Use default pc_id
    return args["pc_id"]

def load_finished_exp(args):
    try:
        with open("finished_exp.txt","r") as f:
            finished_exp = f.readlines()
            finished_exp = [f.split(" ")[0].strip() for f in finished_exp]
    except:
        finished_exp = []
    return finished_exp

def git_pull(args):
    if args["auto_git_pull"] == False: return

    command = "git pull"
    p = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, bufsize=1)
    for line in iter(p.stdout.readline, b''):
        print(line.decode("utf-8"))
    
    p.wait() # Without this p.returncode will be None

def send_email(content, args):
    gmail_user = args["gmail_username"]
    gmail_password = args["gmail_password"]

    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.ehlo()
    server.starttls()

    to = args["send_to"].split(",")
    to = [item.strip() for item in to]

    email_text = content

    server.login(gmail_user, gmail_password)
    server.sendmail(gmail_user, to, email_text)
    server.close()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('--src_folder', type=str, default="src")
    parser.add_argument('--exp_folder', type=str, default="exp")
    parser.add_argument('--pc_id', type=str, default="1080Ti")

    # EMAIL CREDENTIALS
    parser.add_argument('--gmail_username', type=str, default="viplab6809@gmail.com")
    parser.add_argument('--gmail_password', type=str, default="viplab68096809")
    parser.add_argument('--send_to', type=str, default='jonathanhans31@gmail.com,daniel.tan@dlsu.edu.ph')
    
    # For sharing
    # parser.add_argument('--gmail_username', type=str, default="you@gmail.com")
    # parser.add_argument('--gmail_password', type=str, default="password123")
    # parser.add_argument('--send_to', type=str, default="re1@gmail.com,re2@gmail.com,re3@gmail.com")

    # FLAGS
    parser.add_argument('--send_email', action="store_true")
    parser.add_argument('--auto_git_pull', action="store_true")

    config = parser.parse_args() 
    args = vars(config)

    args["pc_id"] = load_pc_id(args)
    main(args)


